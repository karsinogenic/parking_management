<?php

use App\Models\Parking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = Parking::all();
    // return $data[0]->plat_nomer;
    return view('welcome')->with("data", $data);
});

Route::post('/ganti_plat', function (Request $request) {
    $data = Parking::find($request->id);
    //dd($data);
    if ($data) {
        Parking::where('id', $request->id)->update(['plat_nomer' => $request->plat, 'is_active' => !$data->is_active]);
    } else {
        Parking::updateOrCreate(['id' => $request->id], ['plat_nomer' => $request->plat]);
    }
    return redirect("/");
});
Route::get('/cari/{id}', function ($id) {
    $data = Parking::find($id);
    return $data;
});
