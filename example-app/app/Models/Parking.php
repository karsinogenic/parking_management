<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parking extends Model
{
    protected $table = "tbl_master_parking";
    protected $guarded = ['id'];

    use HasFactory;
}
