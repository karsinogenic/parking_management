<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_master_parking', function (Blueprint $table) {
            $table->id();
            $table->string("ip_address")->nullable();
            $table->string("plat_nomer")->nullable();
            $table->boolean("is_active")->default(false);
            $table->timestamps();
        });
        DB::table('tbl_master_parking')->insert(['ip_address' => '10.9.12.56', 'plat_nomer' => 'B 1234 CDE', 'is_active' => true]);
        DB::table('tbl_master_parking')->insert(['ip_address' => '10.9.12.120', 'plat_nomer' => 'B 1111 CCC', 'is_active' => false]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_master_parking');
    }
};
