<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style>
        .overlay {
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 9998;
            background: rgba(255, 255, 255, 0.8) url({{ asset('assets/images/red_loader.gif')}}) center no-repeat;
        }

        /* Turn off scrollbar when body element has the loading class */
        body.loading {
            overflow: hidden;
        }

        /* Make spinner image visible when body element has the loading class */
        body.loading .overlay {
            display: block;
        }

        #loader {
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 9999;
            background: rgba(255, 255, 255, 0.8) url({{ asset('assets/images/red_loader.gif')}}) center no-repeat;
        }

        body.loading #loader {
            display: block;
        }

        .custom-scroll {
            overflow-x: auto;
        }

        .even-larger-badge {
            font-size: 1.2em;
        }

        .fa-question-circle:hover,
        .fa-info-circle:hover {
            cursor: help;
            transform: scale(1.1);
        }
    </style>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>


</head>

<body class="hold-transition">
    <div id="loader"></div>
    <div class="overlay"></div>
    
    <div class="content">
        <center>
            <div class="row">
                <label for=""
                style="font-size: 30px;font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif">
                Parking Management</label>
            </div>
        </center>
            <div class="img-parent" style="position: absolute;top:20%;left:20%">
                <img src="/assets/images/DenahTest.png" alt="">
            </div>
            <div id="btn-image-group" style="width: 793px;height: 643px;position: absolute;top:25%">
                @foreach ($data as $list)
                <button class="btn btn-sm {{ $list->is_active?'btn-danger':'btn-success'  }}" onclick={{ $list->is_active?'showRemoveModal(this.value)':'showModal(this.value)' }} value="{{ $list->id }}" style="position: absolute;left:{{($loop->iteration-1)*47.5}}px">{{ $loop->iteration }}</button>
                @endforeach                 
            </div>
            {{-- <div style="background-color: greenyellow;height:20%"></div> --}}
            <div class="row g-3 mt-2" hidden>
                <div class="col">
                    <label for="">Nomer Parkir</label>
                </div>
                <div class="col">
                    <label for="">Plat Nomer</label>
                </div>
                <div class="col">
                    <label for=""></label>
                </div>
            </div>
            <div class="row g-3" hidden>
                <div class="col">
                    <select name="nomer" id="nomer" class="form-select" onchange="cariData(this.value)">
                        @for ($i=1;$i<=36;$i++) <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                    </select>
                </div>
                <div class="col">
                    <input type="text" class="form-control" id="plat" placeholder="Ex: B 1234 CDE" value='{{ $data[0]->plat_nomer ?? 'coba'}}'>
                </div>
                <div class="col">
                    <button id="ubah_btn" type="button" class="btn btn-primary" onclick="ubahPlat1(this.value)"
                        value="http://10.9.12.56/">Change</button>
                    <button id="remove_btn" type="button" class="btn btn-danger" onclick="removePlat1(this.value)"
                        value="http://10.9.12.56/">Remove</button>
                    {{-- <button type="button" class="btn btn-success" onclick="ubahPlat1(this.value)"
                        value="http://10.9.12.56/">Activate</button> --}}
                </div>
            </div>
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="staticBackdropLabel">Tambah Pengunjung</h5>
                    </div>
                    <form method="post" action="/ganti_plat" >
                        @csrf
                    <div class="modal-body">
                        <input type="hidden" name="id" id="hide_id">
                        <div>
                            <label for="">Plat Nomer</label>
                            <input type="text" name="plat" id="plat" class="form-control">
                            <label for="">Ip Address</label>
                            <input type="text" name="ip_addrdess" id="ip_address" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-warning" onclick="gantiIP()">Ganti IP</button>
                        <button type="submit" class="btn btn-success">Assign</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            <div class="modal fade" id="staticBackdrop1" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <form method="post" action="/ganti_plat" >
                        @csrf
                        <div class="modal-body">
                            <input type="hidden" name="id" id="hide_id1">
                            <input type="hidden" name="plat" id="hide_plat1" value="test">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger">Remove</button>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script>
        function onReady(callback) {
            var intervalId = window.setInterval(function() {
                if (document.getElementsByTagName('body')[0] !== undefined) {
                    window.clearInterval(intervalId);
                    callback.call(this);
                }
            }, 1000);
            
        }

        $(document).on({
            ajaxStart: function() {
                $("body").addClass("loading");
            },
            ajaxStop: function() {
                $("body").removeClass("loading");
            }
        });
            
        function setVisible(selector, visible) {
            document.querySelector(selector).style.display = visible ? 'block' : 'none';
        }

        onReady(function() {
            setVisible('#loader', false);
            console.log(window.screen.width-793);
            $('#btn-image-group').css('margin-left',`${(window.screen.width*20/100)+20}px`)
            setVisible('body', true);
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function cariData(value){
            $.ajax({
                url: "/cari/"+value,
                type:"get",
                contentType:"application/json",
                success:function(data){
                    console.log(data);
                    $('#plat').val(data.plat_nomer);
                    $('#ubah_btn').val("http://"+data.ip_address+'/');
                    $('#remove_btn').val(data.ip_address+'/');
                }
            })
        }

        function ubahPlat1(value) {
            let plat = $("#plat").val();
            let new_json = {
                'id':$('#nomer').val(),
                'plat':plat,
            }
            $.ajax({
                url: value+$('#nomer').val()+'. '+plat,
                type: "get",
                contentType:"application/json",
                success: function(){
                    alert("Berhasil");
                }
            })
            $.ajax({
                url: "/ganti_plat",
                type: "post",
                contentType:"application/json",
                data:JSON.stringify(new_json),
                success: function(datax){
                    console.log(datax);
                }
            })

        }

        function showModal(val){
            // console.log(val)
            $("#ip_address").attr("disabled",true);

            $("#staticBackdrop").modal('show');
            $("#hide_id").val(val);
            // $("#hide_plat").val();
        }
        function showRemoveModal(val){
            // console.log(val)
            $("#staticBackdrop1").modal('show');
            $("#hide_id1").val(val);
            $("#hide_plat1").val("test");
        }

        function removePlat1(value) {
            $('#plat').val('');
            let plat = 'Belum Ada';
            let new_json = {
                'id':$('#nomer').val(),
                'plat':plat,
            }
            $.ajax({
                url: value+plat,
                type: "get",
                contentType:"application/json",
                success: function(){
                    alert("Berhasil");
                }
            })
            $.ajax({
                url: "/ganti_plat",
                type: "post",
                contentType:"application/json",
                data:JSON.stringify(new_json),
                success: function(datax){
                    console.log(datax);
                }
            })

        }

        function gantiIP(){
            $("#ip_address").attr("disabled",false);
        }
    </script>
</body>

</html>
